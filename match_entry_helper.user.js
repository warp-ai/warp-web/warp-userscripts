// ==UserScript==
// @name         Warp：試合登録ヘルパ
// @namespace    https://admin.warp-football.jp/
// @version      2019.05.27
// @description  試合登録ヘルパ
// @author       tknrkb
// @homepage     https://gitlab.com/warp-ai/warp-web/warp-userscripts
// @match        https://admin.warp-football.jp/admin/?act=prediction_match_list&*
// @match        https://stg.warp-football.jp/admin/?act=prediction_match_list&*
// @match        http://*/admin/?act=prediction_match_list&*
// @require      https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js
// ==/UserScript==

(function(teams) {
  'use strict';

  //* 1:J1,J2に所属するチームの名前に印をつける
  var add_j1_j2_to_teamname = function() {
    var name = $(this).text().trim();
    //console.log(name);
    if($.inArray(name, teams.j1) >= 0){
      console.log('J1');
      $(this).text('J1：'+name);
    } else if($.inArray(name, teams.j2) >= 0){
      console.log('J2');
      $(this).text('J2：'+name);
    } else if($.inArray(name, teams.j3) >= 0){
      console.log('J3');
      $(this).text('J3：'+name);
    } else {
      // $(this).remove(); // Jリーグ以外は非表示
    }
  };
  $('select[name="val[home_team_id]"] option').each(add_j1_j2_to_teamname);
  $('select[name="val[away_team_id]"] option').each(add_j1_j2_to_teamname);

  //* 2:チーム、スタジアムの選択フォームを select2 化
  // select2のCSSをロード
  $('head link:last').after('<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />');
  // select2
  $('select[name="val[stadium_id]"]').select2({width:"100%"});
  $('select[name="val[home_team_id]"]').select2({width:"100%"});
  $('select[name="val[away_team_id]"]').select2({width:"100%"});

  //
  $('select[name="val[match_start_time_MIN]"] option').each(function() {
    if ($(this).text() % 5 > 0) {
      $(this).remove();
    }
  });
  //* end
  // スクリプト読み込み完了の印
  $('h1.htit').after('+' + GM_info.script.name　+ '：' + GM_info.script.version);
})({
  // TODO:毎年確認すること
  // j1
  /* 2019

     北海道コンサドーレ札幌北海道コンサドーレ札幌
     鹿島アントラーズ鹿島アントラーズ
     ＦＣ東京ＦＣ東京
     横浜Ｆ・マリノス横浜Ｆ・マリノス
     松本山雅ＦＣ松本山雅ＦＣ
     ジュビロ磐田ジュビロ磐田
     ガンバ大阪ガンバ大阪
     ヴィッセル神戸ヴィッセル神戸
     サガン鳥栖サガン鳥栖

     ベガルタ仙台ベガルタ仙台
     浦和レッズ浦和レッズ
     川崎フロンターレ川崎フロンターレ
     湘南ベルマーレ湘南ベルマーレ
     清水エスパルス清水エスパルス
     名古屋グランパス名古屋グランパス
     セレッソ大阪セレッソ大阪
     サンフレッチェ広島サンフレッチェ広島
     大分トリニータ大分トリニータ
  */

  j1: [
    '北海道コンサドーレ札幌',
    '鹿島アントラーズ',
    'ＦＣ東京',
    '横浜Ｆ・マリノス',
    '松本山雅ＦＣ',

    'ジュビロ磐田',
    'ガンバ大阪',
    'ヴィッセル神戸',
    'サガン鳥栖',
    'ベガルタ仙台',

    '浦和レッズ',
    '川崎フロンターレ',
    '湘南ベルマーレ',
    '清水エスパルス',
    '名古屋グランパス',

    'セレッソ大阪',
    'サンフレッチェ広島',
    '大分トリニータ',
  ],
  // j2
  /* 2019
     モンテディオ山形モンテディオ山形
     栃木ＳＣ栃木ＳＣ
     ジェフユナイテッド千葉ジェフユナイテッド千葉
     東京ヴェルディ東京ヴェルディ
     横浜ＦＣ横浜ＦＣ
     アルビレックス新潟アルビレックス新潟
     ＦＣ岐阜ＦＣ岐阜
     ファジアーノ岡山ファジアーノ岡山
     徳島ヴォルティス徳島ヴォルティス
     アビスパ福岡アビスパ福岡
     鹿児島ユナイテッドＦＣ鹿児島ユナイテッドＦＣ

     水戸ホーリーホック水戸ホーリーホック
     大宮アルディージャ大宮アルディージャ
     柏レイソル柏レイソル
     ＦＣ町田ゼルビアＦＣ町田ゼルビア
     ヴァンフォーレ甲府ヴァンフォーレ甲府
     ツエーゲン金沢ツエーゲン金沢
     京都サンガＦ.Ｃ.京都サンガＦ.Ｃ.
     レノファ山口ＦＣレノファ山口ＦＣ
     愛媛ＦＣ愛媛ＦＣ
     Ｖ・ファーレン長崎Ｖ・ファーレン長崎
     ＦＣ琉球ＦＣ琉球

  */
  j2: [
    'モンテディオ山形',
    '栃木ＳＣ',
    'ジェフユナイテッド千葉',
    '東京ヴェルディ',
    '横浜ＦＣ',

    'アルビレックス新潟',
    'ＦＣ岐阜',
    'ファジアーノ岡山',
    '徳島ヴォルティス',
    'アビスパ福岡',

    '鹿児島ユナイテッドＦＣ',
    '水戸ホーリーホック',
    '大宮アルディージャ',
    '柏レイソル',
    'ＦＣ町田ゼルビア',

    'ヴァンフォーレ甲府',
    'ツエーゲン金沢',
    '京都サンガF.C.',
    'レノファ山口ＦＣ',
    '愛媛ＦＣ',

    'Ｖ・ファーレン長崎',
    'ＦＣ琉球',
  ],
  // J3
  /* 2019
     ヴァンラーレ八戸ヴァンラーレ八戸
     ブラウブリッツ秋田ブラウブリッツ秋田
     ザスパクサツ群馬ザスパクサツ群馬
     ＳＣ相模原ＳＣ相模原
     カターレ富山カターレ富山
     アスルクラロ沼津アスルクラロ沼津
     カマタマーレ讃岐カマタマーレ讃岐
     ロアッソ熊本ロアッソ熊本
     ガンバ大阪Ｕ－２３ガンバ大阪Ｕ－２３

     いわてグルージャ盛岡いわてグルージャ盛岡
     福島ユナイテッドＦＣ福島ユナイテッドＦＣ
     Ｙ．Ｓ．Ｃ．Ｃ．横浜Ｙ．Ｓ．Ｃ．Ｃ．横浜
     ＡＣ長野パルセイロＡＣ長野パルセイロ
     藤枝ＭＹＦＣ藤枝ＭＹＦＣ
     ガイナーレ鳥取ガイナーレ鳥取
     ギラヴァンツ北九州ギラヴァンツ北九州
     ＦＣ東京Ｕ－２３ＦＣ東京Ｕ－２３
     セレッソ大阪Ｕ－２３セレッソ大阪Ｕ－２３

  */
  j3: [
    'ヴァンラーレ八戸',
    'ブラウブリッツ秋田',
    'ザスパクサツ群馬',
    'ＳＣ相模原',
    'カターレ富山',

    'アスルクラロ沼津',
    'カマタマーレ讃岐',
    'ロアッソ熊本',
    'ガンバ大阪Ｕ−２３',
    'グルージャ盛岡',

    '福島ユナイテッドＦＣ',
    'Ｙ．Ｓ．Ｃ．Ｃ．横浜',
    'ＡＣ長野パルセイロ',
    '藤枝ＭＹＦＣ',
    'ガイナーレ鳥取',

    'ギラヴァンツ北九州',
    'ＦＣ東京Ｕ−２３',
    'セレッソ大阪Ｕ−２３'
  ]
});
