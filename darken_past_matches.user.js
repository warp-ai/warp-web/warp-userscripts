// ==UserScript==
// @name         Warp：過ぎた予定を薄く表示
// @namespace    https://admin.warp-football.jp/
// @version      2019.05.27
// @description  終った予定を薄く表示
// @license      MIT
// @author       tknrkb
// @match        https://admin.warp-football.jp/admin/?act=prediction_list
// @grant        none
// ==/UserScript==

(function() {
  'use strict';

  var now = new Date();
  console.log(now);
  $('table td.line-id').each(function(){
    var $time = $(this).next().next();
    console.log($time.text());
    var endtime = new Date($time.text());
    console.log(endtime);
    if (now > endtime) {
      console.log('past');
      $time.parent().css({opacity: 0.7});
    }
  });

  //* end
  // スクリプト読み込み完了の印
  $('h1.htit').after('+' + GM_info.script.name　+ '：' + GM_info.script.version);
})();
