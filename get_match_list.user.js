// ==UserScript==
// @name         Warp：対戦リスト
// @namespace    http://tampermonkey.net/
// @version      2019.05.27
// @description  対戦リストをCSVファイルでダウンロード
// @author       tknrkb
// @match        https://admin.warp-football.jp/admin/?act=prediction_match_list&*
// @grant        none
// ==/UserScript==

(function() {
  'use strict';
  var filetitle = $('p.matchInfo_ttl').text().replace(/\s/g,"_");
  console.log(filetitle);

  var score_re = /\d+\s+-\s+\d+/;
  //<script type='text/javascript'>
  function handleDownload() {

    var $table = $('table.table-matchregist');
    var match = {};
    var content = '';
    var cnt = 0;
    var cnt_match = 0;
    $table.find('tr').each( function() {
      var tds = $(this).find('td');

      switch (tds.length) {//CSV content-main table
      case 0:
        content = content + [
          "League",
          "team_home_no", "team_home",
          "team_away_no", "team_away"
        ].join(',') + "\n";
        break;
      case 16:
        match = {
          league: $(tds[3]).text(),
          team_home_no: $(tds[7]).text(),
          team_home: $(tds[8]).text()
        };
        console.log(match);
        break;
      case 6:
        match.team_away_no = $(tds[1]).text();
        match.team_away = $(tds[2]).text();
        console.log(match);
        content = content + [
          match.league,
          match.team_home_no, match.team_home,
          match.team_away_no, match.team_away
        ].join(',') + "\n";
        break;
      default:
        console.log('tds.leg:'+tds.length);
      }
    });


    var bom = new Uint8Array([0xEF, 0xBB, 0xBF]);
    var blob = new Blob([ bom, content ], { "type" : "text/csv" });
    var filename =  'warp_対戦リスト.csv';


    if (window.navigator.msSaveBlob) {
      window.navigator.msSaveBlob(blob, filename);

      // msSaveOrOpenBlobの場合はファイルを保存せずに開ける
      window.navigator.msSaveOrOpenBlob(blob,  filename);
    } else {
      document.getElementById("download_match_list").href = window.URL.createObjectURL(blob);
    }
  }
  window.handleDownload = handleDownload;

  var match_title = $('p.matchInfo_ttl').text().replace(/\s/g,"_");
  $('div.unit').before('<div style="text-align: right;"><a id="download_match_list" href="#" download="warp_対戦リスト_' + match_title + '.csv" onclick="handleDownload()" style="display: inline-block; border: 1px solid black; border-radius: 5px; margin-top: 25px; padding: 3px 5px;">対戦リストをダウンロード</a></div>');
})();
